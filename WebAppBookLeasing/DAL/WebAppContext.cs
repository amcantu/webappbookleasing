﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebAppBookLeasing.Models;

namespace WebAppBookLeasing.DAL
{
    public class WebAppContext: DbContext
    {
        public WebAppContext(): base("name=WebAppContext")
        {
            this.Configuration.LazyLoadingEnabled = false; 
        }

        public DbSet<User> Users { get; set; }
        public DbSet<LeasingTransaction> LeasingTransactions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                        .HasMany<LeasingTransaction>(s => s.Transactions)
                        .WithRequired(s => s.User);
        }
        
   
    }
}