﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAppBookLeasing.DAL;
using WebAppBookLeasing.Models;

namespace WebAppBookLeasing.Controllers
{
    public class LeasingTransactionsController : ApiController
    {
        private WebAppContext db = new WebAppContext();

        // GET: api/LeasingTransactions?UserId=
        public List<LeasingTransaction> Get(int UserId)
        {
            IEnumerable<LeasingTransaction> trans = from tran in db.LeasingTransactions
                                                    where tran.UserID == UserId
                                                    select tran;
            return trans.ToList();
        }
        
        // POST: api/LeasingTransactions
        public void Post(LeasingTransaction value)
        {
            if (ModelState.IsValid)
            {
                db.LeasingTransactions.Add(value);
                db.SaveChanges();
            }
            else
            {
                throw new Exception("Not implemented");
            }
        }

        // PUT: api/LeasingTransactions/5
        public void Put(int id, LeasingTransaction value)
        {
            if (ModelState.IsValid)
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                var tran = from t in db.LeasingTransactions
                           where t.ID == id
                           select t;

                if (tran == null)
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }

                db.Entry(value).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                throw new Exception("Not implemented");
            }
        }

        // DELETE: api/LeasingTransactions/5
        public void Delete(int id)
        {
            var tran = from t in db.LeasingTransactions
                       where t.ID == id
                       select t;

            if (tran == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            db.LeasingTransactions.Remove(tran.First());
            db.SaveChanges();
        }
    }
}
