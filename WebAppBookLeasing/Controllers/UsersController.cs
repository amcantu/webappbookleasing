﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAppBookLeasing.DAL;
using WebAppBookLeasing.Models;

namespace WebAppBookLeasing.Controllers
{
    public class UsersController : ApiController
    {
        private WebAppContext db = new WebAppContext();

        // GET: api/Users
        public List<User> Get()
        {
            IEnumerable<User> users = from user in db.Users
                                      select user;
            return users.ToList();
        }

        // GET: api/Users/5
        public User Get(int id)
        {
            var user = from u in db.Users
                       where u.ID == id
                       select u;
            return user.First();
        }

        // GET: api/Users?NameOrEmail=
        public List<User> Get(string NameOrEmail)
        {
            IEnumerable<User> users =  from u in db.Users
                                       where u.Name.Contains(NameOrEmail) || u.Email.Contains(NameOrEmail)
                                       select u;
            return users.ToList();
        }

        // POST: api/Users
        public void Post(User value)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(value);
                db.SaveChanges();
            }
            else
            {
                throw new Exception("Not implemented");
            }
        }

        // PUT: api/Users/5
        public void Put(int id, User value)
        {
            if (ModelState.IsValid)
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                var user = from u in db.Users
                           where u.ID == id
                           select u;

                if (user == null)
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }

                db.Entry(value).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                throw new Exception("Not implemented");
            }
        }

        // DELETE: api/Users/5
        public void Delete(int id)
        {
            var user = from u in db.Users
                       where u.ID == id
                       select u;

            if (user == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            db.Users.Remove(user.First());
            db.SaveChanges();
        }
    }
}
