﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAppBookLeasing.Models;

namespace WebAppBookLeasing.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            using (UsersController ctrlUser = new UsersController())
            {
                List<User> users;
                users = ctrlUser.Get();
                ViewData["Users"] = users;
            }
            
            return View();
        }
    }
}