namespace WebAppBookLeasing.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using WebAppBookLeasing.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<WebAppBookLeasing.DAL.WebAppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(WebAppBookLeasing.DAL.WebAppContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Users.AddOrUpdate(
                p => p.Name,
                new User { Name = "Kermit A. Estes", Email="Etiam.laoreet.libero@ornareegestas.org", DateOfBirth = DateTime.Parse("1980/11/29"), Phone="(215) 514-8915" },
                new User { Name = "Jerry H. Dillard", Email="Integer.in.magna@posuere.net", DateOfBirth = DateTime.Parse("1990/12/06"), Phone="(640) 987-2718" },
                new User { Name = "Grady J. Kennedy", Email="magna@ataugue.net", DateOfBirth = DateTime.Parse("1992/08/31"), Phone="(587) 159-6000" }
            );
            context.SaveChanges();

            context.LeasingTransactions.AddOrUpdate(t => t.UserID,
                new LeasingTransaction { UserID = 1, BookName = "Harry Potter", StartDate = DateTime.Parse("2015/08/24"), EndDate = DateTime.Parse("2015/12/30"), IsDelivered=false });
        }
    }
}
