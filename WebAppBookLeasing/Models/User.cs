﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebAppBookLeasing.Models
{
    [Table("Users")]
    public class User
    {
        [Key]
        public int ID { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(100)]
        public string Email { get; set; }
        public DateTime DateOfBirth { get; set; }
        [StringLength(15)]
        public string Phone { get; set; }

        public virtual List<LeasingTransaction> Transactions { get; set; }
    }   
}