﻿angular.module("app", ["ngTable"])
.controller('Users', function ($scope, $http, $filter, ngTableParams) {
    $scope.users;
    $scope.userTransactions;
    $scope.addOrUpdateUser;
    $scope.addOrUpdateTransaction;
    $scope.indexSelectedUser;
    $scope.indexDeleteUser;
    $scope.indexDeleteTransaction;
    $scope.filterUser = "";

    $scope.init = function (Users) {
        $scope.users = Users;

        $scope.tableUsers = new ngTableParams(
           {
               page: 1,
               count: 5,
               sorting: { name: 'asc' }
           },
           {
               total: $scope.users.length,
               counts: [],
               filter: [],
               getData: function ($defer, params) {
                   // use build-in angular filter
                   var orderedData = getFilteredSortedData($scope.users, params);
                   params.total(orderedData.length);
                   $defer.resolve(getSlicedData(orderedData, params));
               }
           }
       );

        if ($scope.users.length != 0) {
            $scope.selectUser(0);
        } else {
            $scope.userTransactions = null;
        }
    }
    $scope.searchUser = function () {
        if ($scope.filterUser != "") {
            $http.get("api/Users?NameOrEmail=" + $scope.filterUser)
            .success(function (response) {
                $scope.users = response;
                $scope.tableUsers.reload();
            });
        } else {
            $http.get("api/Users")
            .success(function (response) {
                $scope.users = response;
                $scope.tableUsers.reload();
            });
        }

        if ($scope.users.length != 0) {
            $scope.selectUser(0);
        } else {
            $scope.userTransactions = null;
            $scope.tableTransactions.reload();
        }
    }

    $scope.selectUser = function (index) {
        $scope.indexSelectedUser = index;

        if (index!=null) {
            $http.get("api/LeasingTransactions?UserId=" + $scope.users[index].ID)
            .success(function (response) {
                $scope.userTransactions = response;

                if ($scope.tableTransactions == null) {
                    $scope.tableTransactions = new ngTableParams(
                        {
                            page: 1,
                            count: 5,
                            sorting: { name: 'asc' }
                        },
                        {
                            total: $scope.userTransactions.length,
                            counts: [],
                            filter: [],
                            getData: function ($defer, params) {
                                // use build-in angular filter
                                var orderedData = getFilteredSortedData($scope.userTransactions, params);
                                params.total(orderedData.length);
                                $defer.resolve(getSlicedData(orderedData, params));
                            }
                        }
                    );
                } else {
                    $scope.tableTransactions.reload();
                }
                
                
            });
        }
    }
    $scope.editUser = function (index) {
        $scope.addOrUpdateUser = angular.copy($scope.users[index]);
        $('#modalUser').modal('show');
    }
    $scope.createUser = function () {
        var dateOfBirth = new Date();
        $scope.addOrUpdateUser = { ID: 0, Name: '', DateOfBirth: dateOfBirth, Email: '', Phone: '' };
        $('#modalUser').modal('show');
    }
    $scope.cancelUserAddOrUpdate = function () {
        $scope.addOrUpdateUser = null;
        $('#modalUser').modal('hide');
    }
    $scope.submitUserAddOrUpdate = function () {
        if ($scope.addOrUpdateUser.ID == 0) {
            //Create
            $http.post('/api/Users', $scope.addOrUpdateUser)
                .success(function (data, status, headers) {
                    alert("User saved.");

                    $http.get("api/Users/")
                    .success(function (response) {
                        $scope.users = response;
                        $scope.tableUsers.reload();
                    });

                    $scope.cancelUserAddOrUpdate();
                })
                .error(function (data, status, header, config) {
                    alert("Error on submiting new user.");
                    $scope.cancelUserAddOrUpdate();
                });
        } else {
            //Edit
            $http.put('/api/Users/' + $scope.addOrUpdateUser.ID, $scope.addOrUpdateUser)
                .success(function (data, status, headers) {
                    alert("User saved.");

                    $http.get("api/Users/")
                    .success(function (response) {
                        $scope.users = response;
                        $scope.tableUsers.reload();
                    });

                    $scope.cancelUserAddOrUpdate();
                })
                .error(function (data, status, header, config) {
                    alert("Error on submiting edited user.")
                    $scope.cancelUserAddOrUpdate();
                });
        }
            
    }
    $scope.deleteUser = function (index) {
        $scope.indexDeleteUser = index;
        $('#modalUserDelete').modal('show')
    }
    $scope.cancelUserDelete = function () {
        $scope.indexDeleteUser = null;
        $('#modalUserDelete').modal('hide');
    }
    $scope.submitUserDelete = function () {
        if ($scope.indexDeleteUser != null) {
            $http.delete('/api/Users/' + $scope.users[$scope.indexDeleteUser].ID)
                .success(function (data, status, headers) {
                    alert("User deleted.");

                    $http.get("api/Users/")
                    .success(function (response) {
                        $scope.users = response;
                        $scope.tableUsers.reload();
                    });

                    $scope.cancelUserDelete();
                })
                .error(function (data, status, header, config) {
                    alert("Error on deleting user.")
                    $scope.cancelUserDelete();
                });
        }

    }

    $scope.createTransaction = function () {
        var startDate = new Date();
        var endDate = new Date();
        $scope.addOrUpdateTransaction = { ID:0, UserId: $scope.users[$scope.indexSelectedUser].ID, BookName: '', StartDate: startDate, EndDate: endDate, IsDelivered: 0 };
        $('#modalTrans').modal('show');
    }
    $scope.editTransaction = function (index) {
        $scope.addOrUpdateTransaction = angular.copy($scope.userTransactions[index]);
        $('#modalTrans').modal('show');
    }
    $scope.cancelTransactionAddOrUpdate = function () {
        $scope.addOrUpdateTransaction = null;
        $('#modalTrans').modal('hide');
    }
    $scope.submitTransactionAddOrUpdate = function () {
        if ($scope.addOrUpdateTransaction.ID == 0) {
            //Create
            $http.post('/api/LeasingTransactions', $scope.addOrUpdateTransaction)
                .success(function (data, status, headers) {
                    alert("Leasing transaction saved.");

                    $scope.selectUser($scope.indexSelectedUser);

                    $scope.cancelTransactionAddOrUpdate();
                })
                .error(function (data, status, header, config) {
                    alert("Error on submiting new leasing transaction.");
                    $scope.cancelTransactionAddOrUpdate();
                });
        } else {
            //Edit
            $http.put('/api/LeasingTransactions/' + $scope.addOrUpdateTransaction.ID, $scope.addOrUpdateTransaction)
                .success(function (data, status, headers) {
                    alert("Leasing transaction  saved.");

                    $scope.selectUser($scope.indexSelectedUser);

                    $scope.cancelTransactionAddOrUpdate();
                })
                .error(function (data, status, header, config) {
                    alert("Error on submiting new leasing transaction.");
                    $scope.cancelTransactionAddOrUpdate();
                });
        }
    }
    $scope.deleteTransaction = function (index) {
        $scope.indexDeleteTransaction = index;
        $('#modalTransDelete').modal('show');
    }
    $scope.cancelTransactionDelete = function () {
        $scope.indexDeleteTransaction = null;
        $('#modalTransDelete').modal('hide');
    }
    $scope.submitTransactionDelete = function () {
        if ($scope.indexDeleteTransaction != null) {
            $http.delete('/api/LeasingTransactions/' + $scope.userTransactions[$scope.indexDeleteTransaction].ID)
                .success(function (data, status, headers) {
                    alert("Leasing transaction deleted.");

                    $http.get("api/LeasingTransactions?UserId=" + $scope.userTransactions[$scope.indexDeleteTransaction].UserID)
                    .success(function (response) {
                        $scope.userTransactions = response;
                        $scope.tableTransactions.reload();
                    });

                    $scope.cancelTransactionDelete();
                })
                .error(function (data, status, header, config) {
                    alert("Error on deleting leasing transaction.")
                    $scope.cancelTransactionDelete();
                });
        }

    }

    $scope.calculateAge = function (birthday) { 
        var birthDate = new Date(birthday);
        var ageDifMs = Date.now() - birthDate.getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }
    var getFilteredSortedData = function (data, params) {
        if (params.filter()) {
            data = $filter('filter')(data, params.filter());
        }
        if (params) {
            data = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
        }
        return data;
    };
    var getSlicedData = function (data, params) {
        var slicedData = data.slice((params.page() - 1) * params.count(), params.page() * params.count());
        return slicedData;
    };
});